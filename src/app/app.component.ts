import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `
  <h1>Navegadores</h1>
  <h1>{{sug}}</h1>

  <h2>Hora: {{time}}</h2>
  <h2>Navegador utilizado: {{nave}}</h2>
  <h2>{{version}}</h2>
  <h2>{{disp}}</h2>
  `,

  styles: [`
  h1 {color: blue align:center}
  h2 {color: green}
  
  `],
})

export class AppComponent {

time: Date;
nave: String;
chr: boolean; 
moz: boolean;
ope: boolean;
saf: boolean;
edg: boolean;
version: String;
lenguaje: string
disp: String;
sug: String;

constructor(){
  this.chr= (navigator.userAgent.indexOf("Chrome") != -1);
      if (this.chr){
        this.sug= "Prueba mozilla y no tendrás más erorres";
        this.nave ="Google Chrome";
        this.version = "Versión:" + navigator.appVersion;
        this.disp= "Plataforma desde la que se encuentra: " +navigator.platform;
        this.lenguaje = "El lenguaje de su navegador es:" + navigator.language;
        this.time =new Date();
            setInterval(() => this.time = new Date(),1000)
}

  this.moz= (navigator.userAgent.indexOf("Firefox") != -1);
      if (this.moz){
        this.sug= "La mejor opción es siempre Mozilla";
        this.nave ="Mozilla Firefox";
        this.version = "Versión:" + navigator.appVersion;
         this.disp= "Plataforma en el que se encuentra: " +navigator.platform;
        this.lenguaje = "El lenguaje de su navegador es:" + navigator.language;
        this.time =new Date();
            setInterval(() => this.time = new Date(),1000)
}

  this.ope= (navigator.userAgent.indexOf("Opera") != -1);
      if (this.ope){
        this.nave ="Opera";
        this.sug= "Puedes utilizar más navegadores rápidos";
        this.version = "Versión" + navigator.appVersion;
        this.disp= "Plataforma desde la que se encuentra:  " +navigator.platform;
        this.lenguaje = "El lenguaje de su navegador es:" + navigator.language;
        this.time =new Date();
            setInterval(() => this.time = new Date(),1000)
}

  this.saf= (navigator.userAgent.indexOf('safari/') > -1);
     if (this.saf){
      this.nave ="Safari";
      this.version = "Versión:" + navigator.appVersion;
      this.lenguaje = "El lenguaje de su navegador es:" + navigator.language;
      this.disp= "Plataforma desde la que se encuentra: " +navigator.platform;
      this.sug= "Al utilizar este navegador quiere decir que eres rico";
      this.time =new Date();
          setInterval(() => this.time = new Date(),1000)
}

  this.edg= (navigator.userAgent.indexOf("Edge") != -1);
      if (this.edg){
      this.nave ="Microsoft Edge";
      this.version = "Version:" + navigator.appVersion;
      this.lenguaje = "El lenguaje de su navegador es:" + navigator.language;
      this.disp= "Plataforma desde la que se encuentra: " +navigator.platform;
      this.sug= "¿Ha pensado en descargar un navegador mejor?";
  }
}
}
